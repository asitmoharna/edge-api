[
  {
    _id: '5db73fde32ac6f00019c34ff',
    household_token: 'cafebabe',
    location: 'Bangalore, India'
  },
  {
    _id: '5db73fde32ac6f00019c45gg',
    household_token: 'deadbeef',
    location: 'Colombo, Srilanka'
  },
  {
    _id: '5db73fde32ac6f00019c56hh',
    household_token: 'c0de0123',
    location: 'Berlin, Germany'
  },
].each do |attr|
    t = Thermostat.where(attr).first
    Thermostat.create(attr) unless t
  end
