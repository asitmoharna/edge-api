module ExceptionHandler
  def self.included(klass)
    klass.class_eval do
      rescue_from ArgumentError, with: :bad_request
      rescue_from Base::InvalidRecordError, with: :bad_request
      rescue_from Base::UnknownRecordError, with: :not_found
      rescue_from ApplicationController::BadCredentialsError, with: :unauthorized
    end
  end

  private

  def bad_request(e)
    render json: e.message, status: :bad_request
  end

  def not_found(e)
    render json: e.message, status: :not_found
  end

  def unauthorized(e)
    render json: e.message, status: :unauthorized
  end
end
