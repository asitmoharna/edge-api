Redis.current = Redis.new({
  host: ENV["redis-host"] || "redis",
  port: ENV["redis-port"] || 6379,
  db: ENV["redis-db"] || 0
})
