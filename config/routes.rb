Rails.application.routes.draw do
  scope :v1, defaults: { format: :json } do
    get 'ping', to: 'home#ping'

    resources :thermostat, only: []  do
      member do
        get 'stats'
      end
      resources :readings, only: [:create, :show]
    end
  end
end
