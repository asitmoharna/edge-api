class Base
  class InvalidRecordError < StandardError; end
  class UnknownRecordError < StandardError; end
end
