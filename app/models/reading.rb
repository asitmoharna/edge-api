class Reading < Base
  include Mongoid::Document
  include Redis::Objects

  belongs_to :thermostat

  field :tracking_number, type: :integer
  field :temperature, type: :float
  field :humidity, type: :float
  field :battery_charge, type: :float

  validates :temperature, :humidity, :battery_charge, presence: true
  validates :thermostat_id, presence: true

  validates :tracking_number, presence: true,
    on: :generated_number
  validates :tracking_number, uniqueness: { scope: :thermostat_id },
    on: :generated_number

  # NOTE: Change this method when attributes changes for this model.
  def to_attr
    {
      tracking_number: self.tracking_number,
      thermostat_id: self.thermostat_id.to_s,
      temperature: self.temperature,
      battery_charge: self.battery_charge,
      humidity: self.humidity
    }
  end

  def to_json
    self.to_attr.to_json
  end

  def ingest
    save_to_redis
    save_to_db_later
    update_stats
  end

  def update_stats
    self.thermostat.update_stats(
      temperature: self.temperature,
      humidity: self.humidity,
      battery_charge: self.battery_charge
    )
  end

  def save_to_redis
    raise InvalidRecordError.new self.errors.full_messages unless valid?

    self.thermostat.readings_count.increment do |nxt_number|
      self.tracking_number = nxt_number

      if self.valid?(:generated_number)
        # save to redis
        # TODO: Serialize only few attributes
        self.redis.set(self.redis_document_key, self.to_attr.to_json)
      else
        # abort
        self.thermostat.readings_count.decrement
        raise InvalidRecordError.new self.errors.full_messages
      end
    end
  end

  # NOTE: Keep the keys in such a way that it can be easily extensible and moved
  # to a separate module in future.
  def redis_document_key
    "#{self.class.name.downcase}:#{self.thermostat_id}:#{self.tracking_number}"
  end

  def save_to_db_later
    SaveReadingToDbJob.perform_later self.to_attr
  end

  def self.fast_find(tracking_number:, thermostat_id:)
    reading_from_redis = Reading.redis.get("reading:#{thermostat_id}:#{tracking_number}")
    return JSON.parse(reading_from_redis) if reading_from_redis

    reading = Reading.where(tracking_number: tracking_number, themrostat_id: thermostat_id).first
    raise UnknownRecordError unless reading

    reading
  end
end
