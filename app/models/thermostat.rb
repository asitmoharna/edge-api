class Thermostat < Base
  include Mongoid::Document
  include Redis::Objects

  has_many :readings

  field :household_token
  field :location

  value :token
  counter :readings_count, start: 0
  hash_key :stats

  validates :household_token, :location, presence: true
  validates :household_token, uniqueness: true

  after_create do
    # Put it in the cache so that it will be readily available for
    # authentication
    self.token.set self.household_token
  end

  def update_stats(temperature:, humidity:, battery_charge:)
    existing = self.stats.to_h

    set_stats(temperature: temperature,
              humidity: humidity,
              battery_charge: battery_charge) and return if existing.empty?

    new_temperature = eval(existing['temperature']).tap do |hsh|
      hsh[:min] = temperature if temperature < hsh[:min]
      hsh[:max] = temperature if temperature > hsh[:max]
      hsh[:sum] += temperature
    end

    new_humidity = eval(existing['humidity']).tap do |hsh|
      hsh[:min] = humidity if humidity < hsh[:min]
      hsh[:max] = humidity if humidity > hsh[:max]
      hsh[:sum] += humidity
    end

    new_battery_charge = eval(existing['battery_charge']).tap do |hsh|
      hsh[:min] = battery_charge if battery_charge < hsh[:min]
      hsh[:max] = battery_charge if battery_charge > hsh[:max]
      hsh[:sum] += battery_charge
    end

    self.stats.bulk_set(
      temperature: new_temperature,
      humidity: new_humidity,
      battery_charge: new_battery_charge
    )
  end

  def self.stats_for(id)
    Thermostat.redis.hgetall("thermostat:#{id}:stats")
  end

  private

  def set_stats(temperature:, humidity:, battery_charge:)
    self.stats.bulk_set(
      temperature: { min: temperature, max: temperature, sum: temperature},
      humidity: { min: humidity, max: humidity, sum: humidity},
      battery_charge: { min: battery_charge, max: battery_charge, sum: battery_charge},
    )
  end
end
