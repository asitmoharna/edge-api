class ThermostatController < ApplicationController
  def stats
    result = Thermostat.stats_for(params[:id])

    render json: result, status: :ok
  end
end
