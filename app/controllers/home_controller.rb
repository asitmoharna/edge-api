class HomeController < ApplicationController
  skip_before_action :authenticate

  def ping
    render json: 'pong', status: :ok
  end
end
