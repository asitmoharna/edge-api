class ApplicationController < ActionController::API
  class BadCredentialsError < StandardError; end
  before_action :authenticate

  include ExceptionHandler

  private

  def authenticate
    token = params[:token]
    thermostat_id = params[:thermostat_id] || params[:id]

    raise ArgumentError.new('missing_token') if token.nil?
    raise BadCredentialsError.new('invalid_token') if Redis.current.get("thermostat:#{thermostat_id}:token") != token
  end
end
