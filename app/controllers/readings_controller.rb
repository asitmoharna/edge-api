class ReadingsController < ApplicationController
  def create
    reading = Reading.new(create_reading_params)

    reading.ingest
    render json: reading.to_json, status: :created
  end

  def show
    reading = Reading.fast_find(
      thermostat_id: thermostat_id,
      tracking_number: tracking_number
    )

    render json: reading.to_json, status: :ok
  end

  private

  def tracking_number
    permitted = params.permit(:token, :thermostat_id, :id)

    permitted.fetch(:id)
  end

  def thermostat_id
    permitted = params.permit(:token, :thermostat_id, :id)

    permitted.fetch(:thermostat_id)
  end

  def create_reading_params
    allowed_reading_params.slice(:thermostat_id, :temperature, :humidity, :battery_charge)
  end

  def allowed_reading_params
    params.permit(:token, :thermostat_id, :temperature, :humidity, :battery_charge)
  end
end
