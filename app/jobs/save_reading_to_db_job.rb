class SaveReadingToDbJob < ApplicationJob
  queue_as :default

  def perform(attrs)
    Reading.new(attrs).save
  end
end
