# Edge API
An API supporting Thermostats reading

# Objective
Create a performant API for ingesting readings from thermostats and consistently
provide reading data and stats for those.

# How it works
This project uses Redis as a primary ingestor for readings data and MongoDB
as an eventual data store. Uses Sidekiq for delaying saving to DB.

# Development
This project uses docker and docker-compose to maintain dependencies.

1. Clone the project
2. Build the project `docker-compose build`
3. Run Test cases `docker-compose run web rspec spec`
4. Seed the thermostats `docker-compose run web rake db:seed`
5. Run the project `docker-compose up [-d]`

6. API endpoints are as follows
```
               ping GET  /v1/ping(.:format)                                   home#ping {:format=>:json}
   stats_thermostat GET  /v1/thermostat/:id/stats(.:format)                   thermostat#stats {:format=>:json}
thermostat_readings POST /v1/thermostat/:thermostat_id/readings(.:format)     readings#create {:format=>:json}
 thermostat_reading GET  /v1/thermostat/:thermostat_id/readings/:id(.:format) readings#show {:format=>:json}
 ```

7. Sample Requests

- Health check URL
```
curl -X GET http://localhost:3000/v1/ping
```

- Create readings URL
```
curl -i -X POST http://localhost:3000/v1/thermostat/5db73fde32ac6f00019c34ff/readings -d 'temperature=20.0&humidity=30.0&battery_charge=40.0&token=cafebabe'
```

- Get readings URL
```
curl -i -X GET
http://localhost:3000/v1/thermostat/5db73fde32ac6f00019c34ff/readings/1 -d 'token=cafebabe'
```

- Get stats URL
```
curl -i -X GET
http://localhost:3000/v1/thermostat/5db73fde32ac6f00019c34ff/stats -d 'token=cafebabe'
```

# Author
[Asit Moharna](https://twitter.com/asitmoharna)
