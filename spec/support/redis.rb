RSpec.configure do |config|
  config.before(:each) do |example|
    Redis.current.flushall
  end

  config.after(:each) do |example|
    Redis.current.flushall
  end
end
