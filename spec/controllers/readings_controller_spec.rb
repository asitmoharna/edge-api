require 'rails_helper'

RSpec.describe ReadingsController, type: :controller do
  describe '#create' do
    let(:thermostat) { FactoryBot.create(:thermostat) }

    context 'when required parameters are missing' do
      it 'responds with a bad request' do
        post :create, params: { thermostat_id: thermostat.id }

        expect(response.code).to eq('400')
      end

      it 'temperature is not given responds with a bad request' do
        post :create, params: {
          token: thermostat.household_token,
          thermostat_id: thermostat.id,
          reading: {
            humidity: 10.0,
            battery_charge: 10.0
          }
        }

        expect(response.code).to eq('400')
      end

      it 'humidity is not given responds with a bad request' do
        post :create, params: {
          token: thermostat.household_token,
          thermostat_id: thermostat.id,
          reading: {
            temperature: 10.0,
            battery_charge: 10.0
          }
        }

        expect(response.code).to eq('400')
      end

      it 'battery_charge is not given responds with a bad request' do
        post :create, params: {
          token: thermostat.household_token,
          thermostat_id: thermostat.id,
          reading: {
            temperature: 10.0,
            humidity: 10.0
          }
        }

        expect(response.code).to eq('400')
      end

      it 'token is not given responds with a bad request' do
        post :create, params: {
          thermostat_id: thermostat.id,
          reading: {
            temperature: 10.0,
            humidity: 10.0,
            battery_charge: 10.0
          }
        }

        expect(response.code).to eq('400')
      end
    end

    context 'when authentication token mis-matches' do
      it 'saves the reading to redis' do
        post :create, params: {
          thermostat_id: thermostat.id,
          token: 'some_random_token',
          temperature: 10.0,
          humidity: 20.0,
          battery_charge: 30.0
        }

        expect(response.code).to eq('401')
        expect(response.body).to eq('invalid_token')
      end
    end

    context 'when all parameters given' do
      it 'saves the reading to redis' do
        post :create, params: {
          thermostat_id: thermostat.id,
          token: thermostat.household_token,
          temperature: 10.0,
          humidity: 20.0,
          battery_charge: 30.0
        }

        expect(response.code).to eq('201')

        body = JSON.parse(response.body)
        expect(body.fetch('tracking_number')).to eq(1)
        expect(body.fetch('temperature')).to eq(10.0)
        expect(body.fetch('humidity')).to eq(20.0)
        expect(body.fetch('battery_charge')).to eq(30.0)
      end
    end
  end

  describe '#show' do
    let(:thermostat) { FactoryBot.create(:thermostat) }
    let(:reading) { FactoryBot.create(:reading, thermostat: thermostat) }

    context 'when required parameters are missing' do
      it 'responds with a bad request' do
        get :show, params: {
          thermostat_id: thermostat.id,
          id: reading.tracking_number,
        }

        expect(response.code).to eq('400')
      end
    end

    context 'when authentication token mis-matches' do
      it 'responds with unauthorized' do
        get :show, params: {
          thermostat_id: thermostat.id,
          token: 'some_random_token',
          id: reading.tracking_number
        }

        expect(response.code).to eq('401')
        expect(response.body).to eq('invalid_token')
      end
    end

    context 'when all parameters given' do
      it 'fetches the reading from redis' do
        thermostat = FactoryBot.create(:thermostat)
        reading = FactoryBot.create(:reading, thermostat: thermostat,
                                 temperature: 10.0, humidity: 20.0,
                                 battery_charge: 30.0, tracking_number: 1)

        reading.redis.set(reading.redis_document_key, reading.to_json)

        get :show, params: {
          thermostat_id: thermostat.id,
          token: thermostat.household_token,
          id: reading.tracking_number
        }

        expect(response.code).to eq('200')

        body = JSON.parse(response.body)
        expect(body.fetch('tracking_number')).to eq(1)
        expect(body.fetch('temperature')).to eq(10.0)
        expect(body.fetch('humidity')).to eq(20.0)
        expect(body.fetch('battery_charge')).to eq(30.0)
      end
    end
  end
end
