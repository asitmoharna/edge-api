require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe '#ping' do
    it 'responds with a pong' do
      get :ping

      expect(response.code).to eq('200')
      expect(response.body).to eq('pong')
    end
  end
end
