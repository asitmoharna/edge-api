require 'rails_helper'

RSpec.describe ThermostatController, type: :controller do
  describe '#stats' do
    let(:thermostat) { FactoryBot.create(:thermostat) }

    context 'when auth token is missing' do
      it 'responds with bad request' do
        get :stats, params: { id: thermostat.id.to_s }

        expect(response.code).to eq('400')
      end
    end

    context 'when auth token mis-matches' do
      it 'responds with unauthorized' do
        get :stats, params: { id: thermostat.id.to_s,
                              token: 'something_else'}

        expect(response.code).to eq('401')
      end
    end

    context 'when auth token is valid' do
      it 'responds with empty stats without readings' do
        get :stats, params: { id: thermostat.id.to_s,
                              token: thermostat.household_token }

        expect(response.code).to eq('200')
        expect(response.body).to eq('{}')
      end

      it 'responds with actual stats with readings' do
        reading = FactoryBot.create(:reading, thermostat: thermostat)
        reading.ingest

        get :stats, params: { id: thermostat.id.to_s,
                              token: thermostat.household_token }

        expect(response.code).to eq('200')
        expect(response.body).to_not eq('{}')

        expect(response.body).to include("temperature")
        expect(response.body).to include("humidity")
        expect(response.body).to include("battery_charge")
      end
    end
  end
end
