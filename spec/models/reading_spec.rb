require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe Reading, type: :model do
  it 'exists' do
    expect { Reading }.not_to raise_error
  end

  describe 'attributes' do
    let(:reading) { FactoryBot.build(:reading) }

    it 'has a tracking number' do
      expect(reading.attributes).to include('tracking_number')
    end

    it 'has a temperature' do
      expect(reading.attributes).to include('temperature')
    end

    it 'has a humidity' do
      expect(reading.attributes).to include('humidity')
    end

    it 'has a battery_charge' do
      expect(reading.attributes).to include('battery_charge')
    end

    it 'has thermostat_id' do
      expect(reading.attributes).to include('thermostat_id')
    end
  end

  describe 'validations' do
    it 'validates presence of temperature' do
      reading = FactoryBot.build(:reading, temperature: nil)
      expect(reading.valid?).to be false

      expect(reading.errors).to include(:temperature)
      expect(reading.errors.full_messages_for(:temperature)).to include("Temperature can't be blank")
    end

    it 'validates presence of humidity' do
      reading = FactoryBot.build(:reading, humidity: nil)
      expect(reading.valid?).to be false

      expect(reading.errors).to include(:humidity)
      expect(reading.errors.full_messages_for(:humidity)).to include("Humidity can't be blank")
    end

    it 'validates presence of battery charge' do
      reading = FactoryBot.build(:reading, battery_charge: nil)
      expect(reading.valid?).to be false

      expect(reading.errors).to include(:battery_charge)
      expect(reading.errors.full_messages_for(:battery_charge)).to include("Battery charge can't be blank")
    end

    it 'validates presence of thermostat id' do
      reading = FactoryBot.build(:reading, thermostat_id: nil)
      expect(reading.valid?).to be false

      expect(reading.errors).to include(:thermostat_id)
      expect(reading.errors.full_messages_for(:thermostat_id)).to include("Thermostat can't be blank")
    end

    it 'bypasses presence of tracking number by default' do
      reading = FactoryBot.build(:reading, tracking_number: nil)
      expect(reading.valid?).to be true
    end

    it 'validates presence of tracking number in the context of generated number' do
      reading = FactoryBot.build(:reading, tracking_number: nil)
      expect(reading.valid?(:generated_number)).to be false

      expect(reading.errors).to include(:tracking_number)
      expect(reading.errors.full_messages_for(:tracking_number)).to include("Tracking number can't be blank")
    end

    it 'validates uniqueness of tracking number for thermostat in the context of generated number' do
      thermostat = FactoryBot.create(:thermostat)

      reading1 = FactoryBot.create(:reading, tracking_number: 1, thermostat: thermostat)
      reading2 = FactoryBot.build(:reading, tracking_number: 1, thermostat: thermostat)
      expect(reading2.valid?(:generated_number)).to be false

      expect(reading2.errors).to include(:tracking_number)
      expect(reading2.errors.full_messages_for(:tracking_number)).to include("Tracking number is already taken")
    end

    it 'allows same tracking number for different thermostats' do
      thermostat = FactoryBot.create(:thermostat)
      thermostat2 = FactoryBot.create(:thermostat)

      reading1 = FactoryBot.create(:reading, tracking_number: 1, thermostat: thermostat)
      reading2 = FactoryBot.build(:reading, tracking_number: 1, thermostat: thermostat2)
      expect(reading2.valid?(:generated_number)).to be true
    end
  end

  describe '#save_to_redis' do
    it 'does not save to redis without valid thermostat' do
      reading = FactoryBot.build(:reading, thermostat_id: 'doesnotexist')

      expect(reading.valid?).to eq(false)

      expect {
        reading.save_to_redis
      }.to raise_error(Reading::InvalidRecordError)

      expect(reading.redis.get(
        reading.redis_document_key
      )).to be(nil)
    end

    it 'does not save to redis without temperature' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, temperature: nil)

      expect(reading.valid?).to eq(false)

      expect {
        reading.save_to_redis
      }.to raise_error(Reading::InvalidRecordError)

      expect(reading.redis.get(
        reading.redis_document_key
      )).to be(nil)
    end

    it 'does not save to redis without humidity' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, humidity: nil)

      expect(reading.valid?).to eq(false)

      expect {
        reading.save_to_redis
      }.to raise_error(Reading::InvalidRecordError)

      expect(reading.redis.get(
        reading.redis_document_key
      )).to be(nil)
    end

    it 'raises invalid record error without battery charge' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, battery_charge: nil)

      expect(reading.valid?).to eq(false)

      expect {
        reading.save_to_redis
      }.to raise_error(Reading::InvalidRecordError)

      expect(reading.redis.get(
        reading.redis_document_key
      )).to be(nil)
    end

    it 'saves reading to redis' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, thermostat: thermostat)

      expect(reading.thermostat.readings_count).to eq(0)

      reading.save_to_redis

      expect(reading.thermostat.readings_count).to eq(1)

      expect(reading.redis.get(
        reading.redis_document_key
      )).to eq(reading.to_attr.to_json)
    end
  end

  describe '#ingest' do
    it 'saves reading to redis' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, thermostat: thermostat)

      expect(reading).to receive(:save_to_redis) { 1.times }

      reading.ingest
    end

    it 'increments the reading count for the thermostat' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, thermostat: thermostat)

      expect(reading.thermostat.readings_count).to eq(0)

      reading.ingest

      expect(reading.thermostat.readings_count).to eq(1)
    end

    it 'schedules saving of reading to DB' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, thermostat: thermostat)

      expect(reading.thermostat.readings_count.value).to eq(0)

      expect(SaveReadingToDbJob).to receive(:perform_later)

      reading.ingest

      expect(reading.thermostat.readings_count.value).to eq(1)

      expect(reading.redis.get(
        reading.redis_document_key
      )).to eq(reading.to_attr.to_json)
    end

    it 'sets stats for the thermostat' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, thermostat: thermostat)

      expect(reading.thermostat.stats.to_h).to be_empty

      reading.ingest

      after_stats = reading.thermostat.stats.to_h

      expect(
        after_stats['temperature']
      ).to eq({
        min: reading.temperature,
        max: reading.temperature,
        sum: reading.temperature
      }.to_s)

      expect(
        after_stats['humidity']
      ).to eq({
        min: reading.humidity,
        max: reading.humidity,
        sum: reading.humidity
      }.to_s)

      expect(
        after_stats['battery_charge']
      ).to eq({
        min: reading.battery_charge,
        max: reading.battery_charge,
        sum: reading.battery_charge
      }.to_s)
    end

    it 'updates stats for the thermostat' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.build(:reading, thermostat: thermostat)

      expect(reading.thermostat.stats.to_h).to be_empty

      reading.ingest

      after_stats = reading.thermostat.stats.to_h

      expect(
        after_stats['temperature']
      ).to eq({
        min: reading.temperature,
        max: reading.temperature,
        sum: reading.temperature
      }.to_s)

      expect(
        after_stats['humidity']
      ).to eq({
        min: reading.humidity,
        max: reading.humidity,
        sum: reading.humidity
      }.to_s)

      expect(
        after_stats['battery_charge']
      ).to eq({
        min: reading.battery_charge,
        max: reading.battery_charge,
        sum: reading.battery_charge
      }.to_s)
    end
  end

  describe '.fast_find' do
    it 'tries to find data from redis' do
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.create(:reading, thermostat: thermostat,
                                  temperature: 10.0, humidity: 20.0,
                                  battery_charge: 30.0, tracking_number: 1000)
      Redis.current.set(reading.redis_document_key, reading.to_json)

      reading_found = Reading.fast_find(
        tracking_number: reading.tracking_number,
        thermostat_id: thermostat.id.to_s,
      )

      expect(reading_found.symbolize_keys).to eq(reading.to_attr)
    end

    it 'tries to find data from DB when not found in redis' do
      skip
      thermostat = FactoryBot.create(:thermostat)
      reading = FactoryBot.create(:reading, thermostat: thermostat,
                                  temperature: 10.0, humidity: 20.0,
                                  battery_charge: 30.0, tracking_number: 1000)

      expect(Reading).to receive(:where)

      reading_found = Reading.fast_find(
        tracking_number: reading.tracking_number,
        thermostat_id: thermostat.id.to_s,
      )

      expect(reading_found.symbolize_keys).to eq(reading.to_attr)
    end

    it 'raises error when invalid' do
      expect { 
        reading_found = Reading.fast_find(
          tracking_number: 1,
          thermostat_id: 'invalid_thermostat_id',
        )
      }.to raise_error { Base::UnknownRecordError }
    end
  end
end
