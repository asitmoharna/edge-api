require 'rails_helper'

RSpec.describe Thermostat, type: :model do
  it 'exists' do
    expect { Thermostat }.not_to raise_error
  end

  describe 'attributes' do
    let(:thermostat) { FactoryBot.build(:thermostat) }

    it 'has a household token' do
      expect(thermostat.attributes).to include('household_token')
    end

    it 'has a location' do
      expect(thermostat.attributes).to include('location')
    end
  end

  describe 'redis attributes' do
    let(:thermostat) { FactoryBot.build(:thermostat) }

    it 'has a counter defined for readings' do
      expect(thermostat.readings_count.key).to eq("thermostat:#{thermostat.id}:readings_count")
      expect(thermostat.readings_count.value).to eq(0)
    end

    it 'has stats configured' do
      expect(thermostat.stats.key).to eq("thermostat:#{thermostat.id}:stats")
      expect(thermostat.redis.get(thermostat.stats.key)).to be_nil
    end

    it 'has household token configured' do
      expect(thermostat.token.key).to eq("thermostat:#{thermostat.id}:token")
      expect(thermostat.token.value).to be_nil
    end
  end

  describe 'validations' do
    it 'validates presence of location' do
      thermostat = FactoryBot.build(:thermostat, location: nil)
      expect(thermostat.valid?).to be false

      expect(thermostat.errors).to include(:location)
      expect(thermostat.errors.full_messages_for(:location)).to include("Location can't be blank")
    end

    it 'validates presence of household token' do
      thermostat = FactoryBot.build(:thermostat, household_token: nil)
      expect(thermostat.valid?).to be false

      expect(thermostat.errors).to include(:household_token)
      expect(thermostat.errors.full_messages_for(:household_token)).to include("Household token can't be blank")
    end

    it 'validates uniqueness of household token' do
      thermostat1 = FactoryBot.create(:thermostat, household_token: 'cafebabe')
      thermostat2 = FactoryBot.build(:thermostat, household_token: 'cafebabe')
      expect(thermostat2.valid?).to be false

      expect(thermostat2.errors).to include(:household_token)
      expect(thermostat2.errors.full_messages_for(:household_token)).to include('Household token is already taken')
    end
  end

  describe 'initializes redis keys after creation' do
    let(:thermostat) { FactoryBot.create(:thermostat) }

    it 'counter gets saved on increment' do
      expect(thermostat.readings_count.key).to eq("thermostat:#{thermostat.id}:readings_count")
      thermostat.readings_count.increment

      expect(thermostat.redis.get(thermostat.readings_count.key)).to eq("1")
      expect(thermostat.readings_count.value).to eq(1)
    end

    it 'does not set stats' do
      expect(thermostat.stats.key).to eq("thermostat:#{thermostat.id}:stats")
      expect(thermostat.stats.to_h).to be_empty
      expect(thermostat.redis.get(thermostat.stats.key)).to be_nil
    end

    it 'sets household token' do
      expect(thermostat.token.key).to eq("thermostat:#{thermostat.id}:token")
      expect(thermostat.token.value).to eq(thermostat.household_token)
    end
  end

  describe '#update_stats' do
    it 'sets the stat first time' do
      thermostat = FactoryBot.create(:thermostat)

      thermostat.update_stats(temperature: 25.0, humidity: 30.0, battery_charge: 100.0)

      expect(thermostat.stats.keys.count).to eq(3)
      expect(thermostat.stats.keys).to include("temperature")
      expect(thermostat.stats.keys).to include("humidity")
      expect(thermostat.stats.keys).to include("battery_charge")

      stats = thermostat.stats.to_h

      expect(stats.count).to eq(3)
      expect(stats["temperature"]).to eq("{:min=>25.0, :max=>25.0, :sum=>25.0}")
      expect(stats["humidity"]).to eq("{:min=>30.0, :max=>30.0, :sum=>30.0}")
      expect(stats["battery_charge"]).to eq("{:min=>100.0, :max=>100.0, :sum=>100.0}")
    end

    it 'updates the stat appropriately' do
      thermostat = FactoryBot.create(:thermostat)

      thermostat.update_stats(temperature: 25.0, humidity: 30.0, battery_charge: 50.0)

      expect(thermostat.stats.keys.count).to eq(3)
      expect(thermostat.stats.keys).to include("temperature")
      expect(thermostat.stats.keys).to include("humidity")
      expect(thermostat.stats.keys).to include("battery_charge")

      stats = thermostat.stats.to_h

      expect(stats.count).to eq(3)
      expect(stats["temperature"]).to eq("{:min=>25.0, :max=>25.0, :sum=>25.0}")
      expect(stats["humidity"]).to eq("{:min=>30.0, :max=>30.0, :sum=>30.0}")
      expect(stats["battery_charge"]).to eq("{:min=>50.0, :max=>50.0, :sum=>50.0}")

      thermostat.update_stats(temperature: 30.0, humidity: 35.0, battery_charge: 55.0)

      stats = thermostat.stats.to_h

      expect(stats.count).to eq(3)
      expect(stats["temperature"]).to eq("{:min=>25.0, :max=>30.0, :sum=>55.0}")
      expect(stats["humidity"]).to eq("{:min=>30.0, :max=>35.0, :sum=>65.0}")
      expect(stats["battery_charge"]).to eq("{:min=>50.0, :max=>55.0, :sum=>105.0}")

      thermostat.update_stats(temperature: 10.0, humidity: 15.0, battery_charge: 45.0)

      stats = thermostat.stats.to_h

      expect(stats.count).to eq(3)
      expect(stats["temperature"]).to eq("{:min=>10.0, :max=>30.0, :sum=>65.0}")
      expect(stats["humidity"]).to eq("{:min=>15.0, :max=>35.0, :sum=>80.0}")
      expect(stats["battery_charge"]).to eq("{:min=>45.0, :max=>55.0, :sum=>150.0}")
    end
  end
end
