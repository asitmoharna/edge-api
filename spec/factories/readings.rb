FactoryBot.define do
  factory :reading do
    thermostat

    tracking_number { rand(1..100) }
    temperature { rand(1.0..100.9) }
    humidity { rand(1.0..100.9) }
    battery_charge { rand(1.0..100.9) }
  end
end
