FactoryBot.define do
  factory :thermostat do
    household_token { SecureRandom.hex }
    location { 'Planet Earth' }
  end
end
